# makefile to set-up the environment for building...

# This is probably only working with GNU make. The file is structured
# to have variables in the beginning, some helper functions, then so
# implicit rules and in the end targets that invoke them all.
#
# If you need to change sysmocom release or poky base branch then the
# two variables need to be changed.
# If you add a machine then MACHINES need to be adjusted and a new config
# in cfg/NAME/10_NAME needs to be created.
#
# Implicit rules: I used WILDCARD-action for the rules and then get the
# name of the rule from $@ in the rule itself or % when still being in
# the dependency list.


# Make everything more verbose by V=1. Taken from kbuild
ifeq ("$(origin V)", "command line")
 Q =
else
 Q = @
endif

# Variables
SYSMOCOM_RELEASE=201705
POKY_RELEASE=pyro
REPOS=poky meta-telephony meta-qt5 meta-sysmocom-bsp meta-smalltalk
MACHINES=sysmobts sysmobts2100 sysmocom-apu2 sysmocom-alix oc2g
FEED_NAME=$(SYSMOCOM_RELEASE)-testing

# The default targets to pick depending on machine
BUILD_TARGET_sysmobts = meta-toolchain-osmo task-sysmocom-feed sysmocom-core-image sysmocom-nitb-image sysmocom-voice-sip-image sysmocom-core-rauc-image sysmocom-nitb-rauc-image image-rauc-rescue-initramfs image-rauc-slot-initramfs image-rauc-ubi
BUILD_TARGET_sysmobts2100 = meta-toolchain-osmo task-sysmocom-feed sysmocom-core-image sysmocom-nitb-image
BUILD_TARGET_oc2g = meta-toolchain-osmo task-sysmocom-feed sysmocom-core-image sysmocom-nitb-image
BUILD_TARGET_sysmocom-apu2 = core-image-minimal-initramfs meta-toolchain-osmo task-sysmocom-feed sysmocom-core-image sysmocom-nitb-image core-image-minimal-initramfs
BUILD_TARGET_sysmocom-alix = core-image-minimal-initramfs meta-toolchain-osmo task-sysmocom-feed sysmocom-core-image sysmocom-nitb-image core-image-minimal-initramfs

# Pick the one depending on $@. Not sure if .SECONDEXPANSION is more
# approiate here or not.
BUILD_TARGETS=$(BUILD_TARGET_$(CUR_MACHINE))

GIT_BRANCH_POKY		= "$(POKY_RELEASE)"
GIT_BRANCH_SYSMOCOM_BSP = "$(SYSMOCOM_RELEASE)"
GIT_BRANCH_TELEPHONY	= "$(SYSMOCOM_RELEASE)"
GIT_BRANCH_SMALLTALK	= "$(SYSMOCOM_RELEASE)"
GIT_BRANCH_QT5		= "$(SYSMOCOM_RELEASE)"

#
usage:
	@echo "Pick a target like help, update or sysmocom-alix-setup"

help:
	@echo "Set-up build environment and execute builds. This is intended"
	@echo "for customers and employees."
	@echo ""
	@echo "Available targets:"
	@echo " usage			- Default target and print usage"
	@echo " help			- This output"
	@echo " update			- git pull --rebase and initial git clone"
	@echo " setup-all		- Set-up all build directories"
	@echo " build-all		- Build all targets"
	@echo " clean-all		- Clean all targets after build"
	@echo " upload-all		- Upload all targets"
	@echo ' install-ssh-config	- Install Host to $$HOME/.ssh/config'
	@echo "Board specific targets:"
	@$(foreach machine, $(MACHINES),		\
		printf " %-16s	- Configure build directory\\n" $(machine)-setup;)
	@$(foreach machine, $(MACHINES),		\
		printf " %-16s	- Configure build directory\\n" $(machine)-build;)
	@$(foreach machine, $(MACHINES),		\
		printf " %-16s	- Configure build directory\\n" $(machine)-upload;)
	@echo "Server targets:"
	@echo " make-server-structure	- Create directories for machine/release"
	@echo "Available variables:"
	@echo " V=1			- Enable verbose command output"
	@echo " SYSMOCOM_RELEASE=name	- Pick release branch during clone"
	@echo " POKY_RELEASE=name	- Pick release branch during clone"
	@echo " GIT_BRANCH_*		- Pick branch for a specific repository"

# Fetch/update all repos... Expand REPOS and append -update to the rule
# e.g. poky-update meta-telephony-update
update: $(foreach repo, $(REPOS), $(repo)-update)

# helper rules

# crazy as I don't know a split()[0]
CUR_MACHINE=$(subst build.,,$(subst -upload,,$(subst -clean,,$(subst -build,,$@))))


## Create a new directory
git:
	$(V)mkdir $@

## Clone repositories. The other option is by variable something like BRNACH_poky, REPO_poky
git/poky: | git
	$(V)cd git && git clone --branch=$(GIT_BRANCH_POKY) --depth=1 https://gitea.sysmocom.de/sysmo-bts/generic-poky poky
git/meta-sysmocom-bsp: | git
	cd git && git clone --branch=$(GIT_BRANCH_SYSMOCOM_BSP) https://gitea.sysmocom.de/sysmo-bts/meta-sysmocom-bsp
git/meta-telephony: | git
	cd git && git clone --branch=$(GIT_BRANCH_TELEPHONY) https://gerrit.osmocom.org/meta-telephony
git/meta-smalltalk: | git
	cd git && git clone --branch=$(GIT_BRANCH_SMALLTALK) https://github.com/sysmocom/meta-smalltalk
git/meta-qt5: | git
	cd git && git clone --branch=$(GIT_BRANCH_QT5) https://github.com/sysmocom/meta-qt5

## Create a build directory, e.g. build.sysmobts
## Use Poky to set-up the directory and then customize it. Copy files
## around and append to the local.conf and layers.conf
CFG_FILES = $(sort $(wildcard cfg/common/*)) $(sort $(wildcard cfg/$(CUR_MACHINE)/*))
build.%: | git/poky
	@echo "Creating build directory for $(CUR_MACHINE)"
	$(Q)/bin/bash -c "source git/poky/oe-init-build-env $@"

	# Append entries to conf/local.conf. Common first, machine second... filter
	$(Q)$(foreach file,$(CFG_FILES), \
		cat $(file) | sed s,BASE_DIR,${CURDIR}, >> $@/conf/local.conf;)
	@echo "require conf/distro/include/sysmocom-defaults.conf" >> $@/conf/local.conf

	$(Q)cat cfg/bblayers.conf | sed  s,BASE_DIR,${CURDIR}, > $@/conf/bblayers.conf


# generic git pull --rebase rule. Let's assume this is matching poky-update
# then the dependency will be "git/poky" and a clone rule will be built.
%-update: | git/$(subst -update,,%)
	@echo "Updating $(subst -update,,$@) ..."
	$(Q)cd git/$(subst -update,,$@) && git pull --rebase

# Setup a build directory
%-setup: | build.$(subst -setup,,%) git/poky
	@echo "Please place proprietary firmware into the downloads directory."

# Start a build..
%-build: | build.$(subst -build,,%) git/poky
	$(Q)/bin/bash -c "source git/poky/oe-init-build-env build.$(CUR_MACHINE) && bitbake $(BUILD_TARGETS) && bitbake package-index"

%-upload: | build.$(subst -upload,,%) git/poky
	$(Q)cd build.$(CUR_MACHINE) && ../scripts/upload-build.sh $(CUR_MACHINE) $(FEED_NAME)

%-clean: | build.$(subst -clean,,%) git/poky
	$(Q)cd build.$(CUR_MACHINE) && ../git/poky/scripts/sstate-cache-management.sh  --cache-dir=sstate-cache -y -L --stamps-dir=tmp/stamps/
	$(Q)cd build.$(CUR_MACHINE) && rm -rf tmp

# Create all build directories, build everything, upload everything, clean everything
setup-all: | $(foreach machine, $(MACHINES), $(machine)-setup)

build-all: | $(foreach machine, $(MACHINES), $(machine)-build)

upload-all: | $(foreach machine, $(MACHINES), $(machine)-upload)

clean-all: | $(foreach machine, $(MACHINES), $(machine)-clean)

make-server-structure:
ifndef WEB_FILES
	$(error "Please call with make make-server-structure WEB_FILES=...")
endif
	$(Q)$(foreach machine, $(MACHINES), \
		mkdir $(WEB_FILES)/$(machine); \
		mkdir $(WEB_FILES)/$(machine)/$(SYSMOCOM_RELEASE); \
		mkdir $(WEB_FILES)/$(machine)/$(SYSMOCOM_RELEASE)-testing; \
		mkdir $(WEB_FILES)/$(machine)/$(SYSMOCOM_RELEASE)-nightly; \
	)


install-ssh-config: | $(HOME)/.ssh
ifndef SSH_HOST
	$(error "Please call with make $@ SSH_HOST=xyz...")
endif
ifndef SSH_PORT
	$(error "Please call with make $@ SSH_PORT=abc...")
endif
ifndef SSH_USER
	$(error "Please call with make $@ SSH_USER=def...")
endif
	@echo "Host = sysmocom-downloads" >> $(HOME)/.ssh/config
	@echo "  HostName = $(SSH_HOST)" >> $(HOME)/.ssh/config
	@echo "  Port = $(SSH_PORT)" >> $(HOME)/.ssh/config
	@echo "  AddressFamily = inet" >> $(HOME)/.ssh/config
	@echo "  User = $(SSH_USER)" >> $(HOME)/.ssh/config
# Target classification
.PHONY: update setup-all install-ssh-config
.SECONDARY: $(foreach repo, $(REPOS), git/$(repo)) $(foreach machine, $(MACHINES), build.$(machine))
