#!/bin/sh

# Dispatch based on SSH_ORIGINAL_COMMAND. rsync, diff and merge.
# TODO: Make this interactive and show the diff before and then
# do the merge?

case "$SSH_ORIGINAL_COMMAND" in
	"rsync")
		exec /usr/local/bin/rrsync web-files
		;;
	"diff-testing")
		exec `dirname $0`/make-stable.sh $1 $2 dry-run
		;;
	"merge-testing")
		exec `dirname $0`/make-stable.sh $1 $2
		;;
esac
